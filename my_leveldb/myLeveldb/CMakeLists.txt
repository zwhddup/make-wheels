cmake_minimum_required(VERSION 3.21)
project(myLeveldb)

set(CMAKE_CXX_STANDARD 11)

add_executable(
        myLeveldb
        main.cpp
        db/node.hpp
        db/skipList.hpp
        db/reusableMemory.cpp db/reusableMemory.h
        db/memTable.cpp db/memTable.h
        db/slice.h db/slice.cpp
        db/test.h
        key/dbformat.cpp key/dbformat.h
        util/coding.cpp util/coding.h
        inte/comparator.h inte/comparator.cpp
        util/testCode.h
        db/writeBatch.cpp db/writeBatch.h
        inte/iterator.cpp inte/iterator.h)
