//
// Created by bit on 2022/11/16.
//

#include "dbformat.h"
#include "../util/coding.h"

//pack SequenceNumber and type
static uint64_t PackSequenceAndType(uint64_t seq, ValueType t) {
    return (seq << 8) | t;
}

LookupKey::LookupKey(const Slice &user_key, SequenceNumber sequence) {
    size_t usize = user_key.size();

}

inline int InternalKeyComparator::Compare(const InternalKey &a,
                                          const InternalKey &b) const {
    return Compare(a.Encode(), b.Encode());
}


int InternalKeyComparator::Compare(const Slice &akey, const Slice &bkey) const {
    //    Order by:
    //    increasing user key (according to user-supplied comparator)
    //    decreasing sequence number
    //    decreasing type (though sequence# should be enough to disambiguate)
    int r = user_comparator_->Compare(ExtractUserKey(akey), ExtractUserKey(bkey));
    if (r == 0) {
        //(SequenceNumber << 8) | Value Type
        const uint64_t anum = DecodeFixed64(akey.data() + akey.size() - 8);
        const uint64_t bnum = DecodeFixed64(bkey.data() + bkey.size() - 8);
        if (anum > bnum) {
            r = -1;
        } else if (anum < bnum) {
            r = +1;
        }
    }
    return r;
}

const char * InternalKeyComparator::Name() const {
    return "myLeveldb.InternalKeyComparator";
}

void InternalKeyComparator::FindShortestSeparator(std::string *start, const Slice &limit) const {
    Slice user_start = ExtractUserKey(*start);
    Slice user_limit = ExtractUserKey(limit);

    std::string tmp(user_start.data(), user_start.size());
    user_comparator_->FindShortestSeparator(&tmp, user_limit);
    if (tmp.size() < user_start.size() && user_comparator_->Compare(user_start, tmp) < 0) {
        // User key has become shorter physically, but larger logically.
        // Tack on the earliest possible number to the shortened user key.
        PutFixed64(&tmp, PackSequenceAndType(kMaxSequenceNumber, kValueTypeForSeek));
/*        assert(this->Compare(*start, tmp) < 0);
        assert(this->Compare(tmp, limit) < 0);*/
        start->swap(tmp);
    }
}

void InternalKeyComparator::FindShortSuccessor(std::string *key) const {
    Slice user_key = ExtractUserKey(*key);
    std::string tmp(user_key.data(), user_key.size());
    user_comparator_->FindShortSuccessor(&tmp);//???
    if (tmp.size() < user_key.size() &&
        user_comparator_->Compare(user_key, tmp) < 0) {
        // User key has become shorter physically, but larger logically.
        // Tack on the earliest possible number to the shortened user key.
        PutFixed64(&tmp, PackSequenceAndType(kMaxSequenceNumber, kValueTypeForSeek));
        //assert(this->Compare(*key, tmp) < 0);
        key->swap(tmp);
    }
}

void AppendInternalKey(std::string *result, const ParsedInternalKey &key) {
    result->append(key.user_key.data(), key.user_key.size());
    PutFixed64(result, PackSequenceAndType(key.sequence, key.type));
}
