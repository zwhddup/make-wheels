//
// Created by bit on 2022/11/16.
//

#ifndef MYLEVELDB_DBFORMAT_H
#define MYLEVELDB_DBFORMAT_H
#include <iostream>
#include <string>
#include "../db/slice.h"
#include "../inte/comparator.h"


typedef uint64_t SequenceNumber;

//0x1ull 为unsigned long long 0x1
static const SequenceNumber kMaxSequenceNumber = ((0x1ull << 56) - 1);

//标记删除 还是插入
enum ValueType {
    kTypeDeletion = 0x0,
    kTypeValue = 0x1
};

static const ValueType kValueTypeForSeek = kTypeValue;

/**
 * InternalKey的拆解
 */
struct ParsedInternalKey {
    Slice user_key;
    SequenceNumber sequence;
    ValueType type;

    ParsedInternalKey() {}
    ParsedInternalKey(const Slice &u, const SequenceNumber &seq, ValueType t)
            : user_key(u), sequence(seq), type(t) {}
    //std::string DebugString() const;
};

void AppendInternalKey(std::string *result, const ParsedInternalKey &key);

//提取userKey
inline Slice ExtractUserKey(const Slice &internal_key) {
    return Slice(internal_key.data(), internal_key.size() - 8);//8bytes
}

/**
 *
 * ┌──────────┬──────────────────────────────────────┐
 * │ User Key │ (Sequence Number << 8) | Value Type  │
 * └──────────┴──────────────────────────────────────┘
 * user_key.size() + 8 byets
 */
class InternalKey {

public:
    InternalKey() {}
    InternalKey(const Slice &user_key, SequenceNumber s, ValueType t) {
        AppendInternalKey(&rep_, ParsedInternalKey(user_key, s, t));
    }

    bool DecodeFrom(const Slice &s) {
        rep_.assign(s.data(), s.size());
        return !rep_.empty();
    }

    Slice Encode() const {
        return rep_;
    }

    Slice user_key() const { return ExtractUserKey(rep_); }

    void SetFrom(const ParsedInternalKey &p) {
        rep_.clear();
        AppendInternalKey(&rep_, p);
    }

    void Clear() { rep_.clear(); };

private:
    std::string rep_;
};

class InternalKeyComparator : public Comparator {

public:
    explicit InternalKeyComparator(const Comparator *c) : user_comparator_(c) {}

    virtual int Compare(const Slice &a, const Slice &b) const override;
    virtual const char * Name() const override;
    virtual void FindShortestSeparator(std::string *start, const Slice &limit) const override;
    virtual void FindShortSuccessor(std::string *key) const override;

    const Comparator * user_comparator() const { return user_comparator_; }

    int Compare(const InternalKey &a, const InternalKey &b) const;
private:
    const Comparator *user_comparator_;
};

/*
 *  ┌───────────────┬─────────────────┬────────────────────────────┐
 *  │ size(varint32)│ User Key(string)│Sequence Number | kValueType│
 *  └───────────────┴─────────────────┴────────────────────────────┘
 *  start_         kstart_                                        end_
 */
class LookupKey {
public:
    LookupKey(const Slice &user_key, SequenceNumber sequence);

    LookupKey(const LookupKey &) = delete;
    LookupKey & operator=(const LookupKey &) = delete;

    ~LookupKey() {
        if (start_ != space_) {
            delete[] start_;
        }
    }

    //整个LookupKey
    Slice memtable_key() const { return Slice(start_, end_ - start_); }

    //user key + sequence number + kValueType
    Slice internal_key() const { return Slice(kstart_, end_ - kstart_); }

    //获取user key
    Slice user_key() const { return Slice(kstart_, end_ - kstart_ - 8); }//8bytes

private:
    // We construct a char array of the form:
    //    klength  varint32               <-- start_
    //    userkey  char[klength]          <-- kstart_
    //    tag      uint64
    //                                    <-- end_
    // The array is a suitable MemTable key.
    // The suffix starting with "userkey" can be used as an InternalKey.
    const char *start_;
    const char *kstart_;
    const char *end_;
    char space_[200];  // Avoid allocation for short keys
};


#endif //MYLEVELDB_DBFORMAT_H
