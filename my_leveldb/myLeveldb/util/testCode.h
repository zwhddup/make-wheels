//
// Created by bit on 2022/11/16.
//

#ifndef MYLEVELDB_TESTCODE_CPP
#define MYLEVELDB_TESTCODE_CPP
#include <iostream>
#include "coding.h"

namespace code {
    /**
     * 测试VarintLength
     */
    void test() {
        //111010101101011010110110011
        std::cout << "len of 123123123: " << VarintLength(123123123ull) << std::endl;
        std::cout << "sizeof(uint64_t): " << sizeof(uint64_t) << std::endl;
        std::cout << "sizeof(int): " << sizeof(int) << std::endl;
        std::cout << "sizeof(uint32_t): " << sizeof(uint32_t) << std::endl;
        std::cout << "sizeof(unsigned long long): " << sizeof(unsigned long long) << std::endl;
        std::cout << "1 << 7: " << (1 << 7) << std::endl;
        std::cout << "257 | 128: " << (257 | 128) << std::endl;
    }

    void test1() {
        char * buf = new char[8];
        EncodeFixed64(buf, 123123123ull);
        cout << "Encode: " << 123123123ull << endl;
        cout << "Decode: " << DecodeFixed64(buf) << endl;
        if (buf != nullptr) {
            delete buf;
            buf = nullptr;
        }
    }
}

#endif //MYLEVELDB_TESTCODE_CPP
