//
// Created by bit on 2022/11/16.
//

#include "coding.h"

//往dst添加uint32
void PutFixed32(std::string *dst, uint32_t value) {
    char buf[sizeof(value)];
    EncodeFixed32(buf, value);
    dst->append(buf, sizeof(buf));
}

//往dst添加uint64
void PutFixed64(std::string *dst, uint64_t value) {
    char buf[sizeof(value)];
    EncodeFixed64(buf, value);
    dst->append(buf, sizeof(buf));
}

//将 value.size 进行编码，并放在 dst 的尾部
//最后将value放在dst尾部
//VarintLength + value
void PutLengthPrefixedSlice(std::string *dst, const Slice &value) {
    PutVarint32(dst, value.size());
    dst->append(value.data(), value.size());
}

//返回v的存储长度(单位为byte)：如uint64: 0000 1101010, 1byte就够存储了
int VarintLength(uint64_t v) {
    int len = 1;
    while (v >= 128) {
        v >>= 7;
        len++;
    }
    return len;
}
/**
 *
 *  VarInt32类型的数据长度是不固定的，VarInt32 中每个字节的最高位有特殊的含义。
 *  如果最高位为 1 代表下一个字节也是该数字的一部分。
 *  因此，表示一个整型数字最少用 1 个字节，最多用 5 个字节表示。
 */
//将v编码成Varint32并写入值dst中
char * EncodeVarint32(char *dst, uint32_t v) {
    // Operate on characters as unsigneds
    uint8_t *ptr = reinterpret_cast<uint8_t *>(dst);
    static const int B = 128;//10000000
    /**
     * 01001010 10101010
     * 低位存储在低地址
    */
    if (v < (1 << 7)) {//10000000
        *(ptr++) = v;
    } else if (v < (1 << 14)) {// 1000000 00000000
        *(ptr++) = v | B;
        *(ptr++) = v >> 7;
    } else if (v < (1 << 21)) {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = v >> 14;
    } else if (v < (1 << 28)) {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = (v >> 14) | B;
        *(ptr++) = v >> 21;
    } else {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = (v >> 14) | B;
        *(ptr++) = (v >> 21) | B;
        *(ptr++) = v >> 28;
    }
    return reinterpret_cast<char *>(ptr);
}

void PutVarint32(std::string *dst, uint32_t v) {
    /* int32 的varint 编码最长为 5 字节 */
    char buf[5];
    char *ptr = EncodeVarint32(buf, v);
    dst->append(buf, ptr - buf);
}

bool GetVarint32(Slice *input, uint32_t *value) {
    const char *p = input->data();
    const char *limit = p + input->size();
    //const char *q GetVarint32Ptr(p, limit, value);
}

//获取Varint32并指针后退
const char * GetVarint32PtrFallback(const char *p, const char *limit, uint32_t *value) {
    uint32_t result = 0;//32bit
    for (uint32_t shift = 0; shift <= 28 && p < limit; shift += 7) {
        uint32_t byte = *(reinterpret_cast<const uint8_t *>(p));//一个字节，一个字节的运算
        ++p;
        if (byte & 128) {//10000000
            // More bytes are present
            //从低位到高位吗？
            result |= ((byte & 127) << shift);// 01111111 <= 127
        } else {
            //读到末尾
            result |= (byte << shift);
            *value = result;
            return reinterpret_cast<const char *>(p);
        }
    }
    return nullptr;
}
