//
// Created by bit on 2022/11/16.
//

#ifndef MYLEVELDB_COMPARATOR_H
#define MYLEVELDB_COMPARATOR_H
#include <string>

class Slice;
/**
 * 比较器 抽象类
 * 四个待实现方法
 */
class Comparator {
public:
    virtual ~Comparator();

    virtual int Compare(const Slice & a, const Slice & b) const = 0;
    virtual const char * Name() const = 0;
    virtual void FindShortestSeparator(std::string * start, const Slice & limit) const = 0;
    virtual void FindShortSuccessor(std::string * key) const = 0;
};

const Comparator * BytewiseComparator();


#endif //MYLEVELDB_COMPARATOR_H
