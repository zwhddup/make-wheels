//
// Created by bit on 2023/3/19.
//

#ifndef MYLEVELDB_ITERATOR_H
#define MYLEVELDB_ITERATOR_H

#include "../db/slice.h"

class Iterator {
public:
    Iterator();

    Iterator(const Iterator &) = delete;
    Iterator& operator=(const Iterator &) = delete;

    virtual ~Iterator();

    virtual bool Valid() const = 0;
    virtual void SeekToFirst() = 0;
    virtual void SeekToLast() = 0;
    virtual void Seek(const Slice &target) = 0;
    virtual bool HasNext() = 0;
    virtual void Next() = 0;
    virtual void Prev() = 0;

    virtual Slice key() const = 0;
    virtual Slice value() const = 0;
};


#endif //MYLEVELDB_ITERATOR_H
