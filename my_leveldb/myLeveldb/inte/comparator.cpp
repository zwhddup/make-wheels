//
// Created by bit on 2022/11/17.
//

#include "comparator.h"
#include "../db/slice.h"

Comparator::~Comparator() = default;

class BytewiseComparatorImpl : public Comparator {
public:
    BytewiseComparatorImpl() = default;
    const char * Name() const override { return "myLeveldb.BytewiseComparator"; }

    //Lexicographic order
    int Compare(const Slice &a, const Slice &b) const override {
        return a.compare(b);
    }



    void FindShortestSeparator(std::string *start, const Slice &limit) const override {
        size_t min_length = std::min(start->size(), limit.size());
        size_t diff_index = 0;
        //判断相等的前缀有多长
        while ((diff_index < min_length) && ((*start)[diff_index] == limit[diff_index])) {
            ++diff_index;
        }
        if (diff_index >= min_length) {
            //一个字符串 是另一个字符串的前缀
        } else {
            uint8_t diff_byte = static_cast<uint8_t>((*start)[diff_index]);
            //0xff => 11111111 => 255
            if (diff_byte < static_cast<uint8_t>(0xff) &&
                diff_byte + 1 < static_cast<uint8_t>(limit[diff_index])) {
                (*start)[diff_index]++;
                start->resize(diff_index + 1);//缩减
            }
        }
    }

    void FindShortSuccessor(std::string *key) const override {
        size_t n = key->size();
        for (size_t i = 0; i < n; ++i) {
            const uint8_t byte = (*key)[i];
            if (byte != static_cast<uint8_t>(0xff)) {
                (*key)[i] = byte + 1;
                key->resize(i + 1);
                return;
            }
        }
    }
};

//实现单列？？？
const Comparator * BytewiseComparator() {
    static BytewiseComparatorImpl singleton;//以后实现
    return &singleton;
}
