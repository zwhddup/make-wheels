//
// Created by bit on 2022/11/15.
//

#ifndef MYLEVELDB_SKIPLIST_HPP
#define MYLEVELDB_SKIPLIST_HPP

#include <mutex>
#include <iostream>
#include <atomic>
#include <cstdlib>
#include <ctime>
#include "node.hpp"
#include "ReusableMemory.h"
using namespace std;

/*
 * hpp文件一般用于函数模版，声明和定义放一处
 * 跳表实现
*/

template <typename Key, class Comparator>
class SkipList {

    const static uint32_t kMaxHeight = 12;

public:
    explicit SkipList(Comparator cmp_, ReusableMemory *rm);
    void Insert(const Key &key);
    //查找是否包含该key
    bool Contains(const Key &key) const;

    //禁止拷贝构造、赋值运算
    SkipList(const SkipList &) = delete;
    SkipList& operator=(const SkipList &) = delete;

    //迭代器
    class Iterator {
    public:
        explicit Iterator(const SkipList *list) : list(list), node(nullptr) {}
        const Key & GetKey() const;
        bool Valid() const;
        void Prev();
        void Seek(const Key &target);
        void SeekToFirst();
        void SeekToLast();
        void Next();
        bool HasNext() const;

    private:
        const SkipList *list;
        Node<Key> *node;
    };


private:
    //比较器
    const Comparator cmp;
    atomic<uint32_t> maxHeight;
    //内存分配器
    ReusableMemory * const reusableMemory;
    Node<Key> * const head;

    //随机选取插入高度
    uint32_t RandomHeight();
    bool Equal(const Key &a, const Key &b) const { return (cmp(a, b) == 0); }
    //key大于n->key则返回true
    bool KeyIsAfterNode(const Key &key, Node<Key> *n) const;
    //用内存分配器分配Node内存
    Node<Key> * NewNode(const Key &key, uint32_t height);
    uint32_t GetMaxHeight() const { return maxHeight; }
    Node<Key> * FindGreaterOrEqual(const Key &key, Node<Key> **prev) const;
    //查找最后一个小于等于key的节点
    Node<Key> * FindLessThan(const Key &key) const;

    Node<Key> * FindLast() const;
};

template <typename Key, class Comparator>
SkipList<Key, Comparator>::SkipList(Comparator cmp_, ReusableMemory *rm)
        : cmp(cmp_),
          maxHeight(1),
          reusableMemory(rm),
          head(NewNode(0, kMaxHeight)) {
    //设置time为seed
    srand(time(0));
    for (int i = 0; i < kMaxHeight; ++i) {
        head->SetNext(i, nullptr);
    }
}

//非空返回true
template <typename Key, class Comparator>
bool SkipList<Key, Comparator>::Iterator::Valid() const {
    return node != nullptr;
}

template <typename Key, class Comparator>
Node<Key> * SkipList<Key, Comparator>::NewNode(const Key &key, uint32_t height) {
    char * const node_memory = reusableMemory->Allocate(
            sizeof(Node<Key>) + sizeof(atomic<Node<Key> *>) * (height - 1));
    //placement new 机制
    return new (node_memory) Node<Key>(key);
}

//返回值1 ~ (kMaxHeight - 1)
template <typename Key, class Comparator>
uint32_t SkipList<Key, Comparator>::RandomHeight() {
    const static uint32_t kBranching = 4;
    //原版随机height，没搞懂，可能就是自己实现的伪随机吧
    //uint32_t height = 1;
    //while (height < kMaxHeight && (rand() % kBranching == 0)) {
    //	height++;
    //}
    //if (height < 0 || height > kMaxHeight) {
    //	cout << "RandomHeight() err: " << height << endl;
    //	//设置默认随机值
    //}
    //return height;
    return rand() % kMaxHeight + 1;
}

template <typename Key, class Comparator>
bool SkipList<Key, Comparator>::Contains(const Key &key) const {
    Node<Key> * x = FindGreaterOrEqual(key, nullptr);
    if (x != nullptr && Equal(key, x->key)) {
        return true;
    } else {
        return false;
    }
}



template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Insert(const Key &key) {
    //存储每一层的大于key的下一个节点
    Node<Key> * prev[kMaxHeight];
    //返回最后一层的大于key的下一个节点
    Node<Key> * x =  FindGreaterOrEqual(key, prev);
    if (x != nullptr && Equal(key, x->key)) {
        cout << "SkipList<Key, Comparator>::Insert(const Key& key) err: duplicate" << endl;
        return;
    }
    uint32_t height = RandomHeight();
    if (height > GetMaxHeight()) {
        //头结点为前驱结点
        for (uint32_t i = GetMaxHeight(); i < height; ++i) {
            prev[i] = head;
        }
        maxHeight = height;
    }
    x = NewNode(key, height);
    //每层节点插入
    for (uint32_t i = 0; i < height; ++i) {
        x->SetNext(i, prev[i]->Next(i));
        prev[i]->SetNext(i, x);
    }
    //cout << "Insert successed to height " << height <<  " : " << key << endl;
    cout << "Insert successed to height " << height << endl;
}

template <typename Key, class Comparator>
bool SkipList<Key, Comparator>::KeyIsAfterNode(const Key &key, Node<Key> *n) const {
    return (n != nullptr) && (cmp(n->key, key) < 0);//判断key是否在这个node后面，比它大
}

template <typename Key, class Comparator>
Node<Key> * SkipList<Key, Comparator>::FindLessThan(const Key &key) const {
    Node<Key> *x = head;
    int level = GetMaxHeight() - 1;
    while (true) {
        Node<Key> *next = x->Next(level);
        if (next == nullptr || cmp(next->key, key) >= 0) {//a >= b return >=0
            if (level == 0) {
                return x;
            } else {
                level--;
            }
        } else {
            x = next;
        }
    }
}

template <typename Key, class Comparator>
Node<Key> * SkipList<Key, Comparator>::FindGreaterOrEqual(const Key &key, Node<Key> **prev) const {
    Node<Key> *x = head;
    //从第0成开始
    int level = GetMaxHeight() - 1;
    while (true) {
        //获取当前level层的下一个节点
        Node<Key>* next = x->Next(level);
        if (KeyIsAfterNode(key, next)) {
            //循环 找到比key大的节点或者到达末尾
            x = next;
        } else {
            //找到了 存储该层比key大的节点的前驱结点
            if (prev != nullptr) {
                prev[level] = x;
            }
            if (level == 0) {
                return next;
            } else {
                //到level层末尾，换下一层找
                level--;
            }
        }
    }
}

template <typename Key, class Comparator>
Node<Key> * SkipList<Key, Comparator>::FindLast() const {
    Node<Key> *x = head;
    int level = GetMaxHeight() - 1;
    while (true) {
        Node<Key> *next = x->Next(level);
        if (next == nullptr) {
            if (level == 0) {
                return x;
            } else {
                // Switch to next list
                level--;
            }
        } else {
            x = next;
        }
    }
}

/*
迭代器
*/
template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Iterator::SeekToFirst() {
    node = list->head->Next(0);
}

template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Iterator::SeekToLast() {
    node = list->FindLast();
    if (node == list->head) {
        node == nullptr;
    }
}



template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Iterator::Next() {
    if (!Valid()) {
        return;
    }
    node = node->Next(0);
}

template <typename Key, class Comparator>
const Key & SkipList<Key, Comparator>::Iterator::GetKey() const {
    return node->key;
}

template <typename Key, class Comparator>
bool SkipList<Key, Comparator>::Iterator::HasNext() const {
    if (!Valid()) {
        return false;
    }
    return true;
}

template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Iterator::Prev() {
    if (!Valid()) {
        return;
    }
    node = list->FindLessThan(node->key);
    if (node == list->head) {
        node = nullptr;
    }
}

template <typename Key, class Comparator>
void SkipList<Key, Comparator>::Iterator::Seek(const Key &target) {
    node = list->FindGreaterOrEqual(target, nullptr);
}

#endif //MYLEVELDB_SKIPLIST_HPP
