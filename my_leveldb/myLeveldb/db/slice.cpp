//
// Created by bit on 2023/3/19.
//
#include "slice.h"

//字典序比较
int Slice::compare(const Slice &a) const {
    const size_t min_len = (size_ < a.size_) ? size_ : a.size_;
    /**
     * memcmp
     * 如果返回值 < 0，则表示 str1 小于 str2。
     * 如果返回值 > 0，则表示 str1 大于 str2。
     * 如果返回值 = 0，则表示 str1 等于 str2。
     */
    int r = memcmp(data_, a.data_, min_len);//存储区 data_ 和存储区 a.data_ 的前min_len个字节进行比较
    if (r == 0) {
        if (size_ < a.size_) {
            r = -1;
        } else if (size_ > a.size_) {
            r = 1;
        }
    }
    return r;
}
