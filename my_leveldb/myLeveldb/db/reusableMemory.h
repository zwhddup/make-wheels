//
// Created by bit on 2022/11/15.
//

#ifndef MYLEVELDB_REUSABLEMEMORY_H
#define MYLEVELDB_REUSABLEMEMORY_H
#include <iostream>
#include <vector>
#include <atomic>
/**
 * 内存分配器
 */
class ReusableMemory {
    static const int kBlockSize = 4096;
public:
    ReusableMemory()
            : alloc_ptr_start(nullptr),
              alloc_bytes_remaining(0),
              memory_usage(0) {}
    ~ReusableMemory();

    ReusableMemory(const ReusableMemory &) = delete;
    ReusableMemory & operator= (const ReusableMemory &) = delete;

    char * Allocate(size_t bytes);
    void PrintMemoryInfo() const;
private:
    //分配的内存开始位置
    char * alloc_ptr_start;
    //剩余可使用的内存
    size_t alloc_bytes_remaining;
    //存储分配的内存
    std::vector<char *> blocks;
    //内存使用总量
    std::atomic<size_t> memory_usage;
    //内存不足，添加新的内存块
    char * AllocateNewBlock(size_t block_bytes);
    //大块就直接分配指定bytes，小块就先分配kBlockSize个bytes
    char * AllocateFallback(size_t bytes);
};


#endif //MYLEVELDB_REUSABLEMEMORY_H
