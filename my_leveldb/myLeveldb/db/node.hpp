//
// Created by bit on 2022/11/15.
//
#ifndef MYLEVELDB_NODE_HPP
#define MYLEVELDB_NODE_HPP

#include <iostream>
#include <atomic>
/**
 * 跳表节点
 * @tparam Key
 */
template <typename Key>
class Node {
public:
    explicit Node(const Key &k) : key(k) {}

    Node * Next(int n) {
        if (n < 0) std::cout << "Node::Next(int n) err: " << n << std::endl;
        return next[n];
    }

    void SetNext(int n, Node *x) {
        if (n < 0) std::cout << "Node::SetNext(int n, Node* x) err: " << n << std::endl;
        next[n] = x;
    }

    //const Key key;
    const Key key;
private:
    //层高
    std::atomic<Node *> next[1];
};
#endif //MYLEVELDB_NODE_HPP
