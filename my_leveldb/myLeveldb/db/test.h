//
// Created by bit on 2022/11/15.
//
#ifndef MYLEVELDB_TEST_CPP
#define MYLEVELDB_TEST_CPP

#include <iostream>
#include "node.hpp"
#include "reusableMemory.h"
#include "skipList.hpp"
#include "slice.h"
#include "../inte/comparator.h"
#include "memTable.h"
#include "../key/dbformat.h"
using namespace std;

//测试内存分配器
ReusableMemory rm;
void test1() {
    cout << "test1" << endl;
    char *bytes = rm.Allocate(4);
    bytes[0] = 'a';
    bytes[1] = 'b';
    bytes[2] = 'c';
    bytes[3] = 'd';
    cout << sizeof(char) << endl;
    cout << strlen(bytes) << endl;
    cout << bytes[0] << endl;
    cout << bytes[1] << endl;
    cout << bytes[2] << endl;
    cout << bytes[3] << endl;
    rm.PrintMemoryInfo();
}

typedef uint64_t Key;

class Comparatortest {
public:
    int operator() (const Key & a, const Key & b) const {
        if (a < b) return -1;
        else if (a > b) return 1;
        return 0;
    }
};

//test Node
void test2() {
    cout << "test2" << endl;
    char * const node_memory = rm.Allocate(
            sizeof(Node<uint64_t>) + sizeof(atomic<Node<uint64_t> *>) * (12 - 1));//16 + 8 * 11
    Node<uint64_t> *node = new (node_memory) Node<uint64_t>(1231223233);
    cout << "node key: " << node->key << endl;
    cout << "sizeof(Node): " << sizeof(Node<int64_t>) << endl;//16 一个int64_t加一个指针
    rm.PrintMemoryInfo();
}

/*
测试skipList
*/
void test3() {
    cout << "test3" << endl;
    Comparatortest cmp;
    SkipList<Key, Comparatortest> list(cmp, & rm);
    list.Insert(123123);
    list.Insert(3423);
    list.Insert(231);
    list.Insert(1);
    list.Insert(56789);
    list.Insert(54);
    SkipList<Key, Comparatortest>::Iterator it(&list);

    //it.Seek(0);
    it.SeekToFirst();
    while (it.HasNext()) {
        cout << it.GetKey() << endl;
        it.Next();
    }
    cout << list.Contains(56789) << endl;
    cout << list.Contains(2) << endl;
}

/*
 * 测试自定义字符串Slice
 */
void test4() {
    cout << "test4" << endl;
    Slice slice("12313");
    Slice slice2("12313432");
    cout << slice.data() << endl;
    cout << (slice != slice2) << endl;
    cout << (slice == slice2) << endl;
    cout << (slice2.compare(slice)) << endl;
    cout << slice[2] << endl;
}
/**
 * test memTable
 */
void test5() {
    SequenceNumber sequence_ = 0;

    const Comparator * cmp = BytewiseComparator();
    InternalKeyComparator icmp(cmp);

    MemTable *m = new MemTable(ref(icmp));
    m->Ref();
    m->Add(++sequence_, kTypeValue, Slice("123"), Slice("456"));
    m->Add(++sequence_, kTypeValue, Slice("hello"), Slice("world!!!"));
    m->Add(++sequence_, kTypeValue, Slice("zwh"), Slice("dd你好！！！ddd"));
    Iterator *it = m->NewIterator();
    it->SeekToFirst();
    while (it->HasNext()) {
        cout << it->value().ToString() << endl;
        it->Next();
    }
    m->Unref();
}

/**
 * FindShortestSeparator 比较
 */
 void test6() {
    const Comparator * cmp = BytewiseComparator();
    string start = {"helloworldcBcdefg"};
    Slice slice = {"helloworldaaedcbad"};
    cmp->FindShortestSeparator(&start, ref(slice));
    cout << start << endl;
 }
#endif