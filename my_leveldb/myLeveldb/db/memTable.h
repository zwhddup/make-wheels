//
// Created by bit on 2022/11/15.
//

#ifndef MYLEVELDB_MEMTABLE_H
#define MYLEVELDB_MEMTABLE_H

#include <iostream>
#include "reusableMemory.h"
#include "../key/dbformat.h"
#include "skipList.hpp"
#include "../key/dbformat.h"
#include "../inte/iterator.h"
class MemTable {
public:

    explicit MemTable(const InternalKeyComparator &comparator);

    MemTable(const MemTable &) = delete;
    MemTable & operator=(const MemTable &) = delete;

    Iterator * NewIterator();

    void Add(SequenceNumber seq, ValueType type, const Slice &key,
             const Slice &value);

    bool Get(const LookupKey &key, std::string *value);

    void Ref() { ++refs_; }

    void Unref() {
        --refs_;
        if (refs_ <= 0) {
            delete this;
        }
    }

private:
    friend class MemTableIterator;

    struct KeyComparator {
        const InternalKeyComparator comparator;
        explicit KeyComparator(const InternalKeyComparator &c) : comparator(c) {}
        int operator()(const char *aptr, const char *bptr) const;
    };

    //使用 Skip List 来实现 MemTable
    typedef SkipList<const char *, KeyComparator> Table;

    KeyComparator comparator_;

    ~MemTable();//引用计数 自动释放；只能用new生成对象

    ReusableMemory rm;

    //引用计数器，管理MemTable的生命周期
    int refs_;
    Table table_;
};



#endif //MYLEVELDB_MEMTABLE_H
