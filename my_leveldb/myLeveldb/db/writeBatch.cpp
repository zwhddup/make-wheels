//
// Created by bit on 2022/11/17.
//

#include "writeBatch.h"
#include "memTable.h"
#include "../util/coding.h"

static const size_t kHeader = 12;

class MemTableInserter : public WriteBatch::Handler {
public:
    SequenceNumber sequence_;
    MemTable *mem_;

    void Put(const Slice &key, const Slice &value) override {
        mem_->Add(sequence_, kTypeValue, key, value);
        sequence_++;
    }

    void Delete(const Slice &key) override {
        mem_->Add(sequence_, kTypeDeletion, key, Slice());
        sequence_++;
    }
};

WriteBatch::WriteBatch() { Clear(); }

void WriteBatch::Clear() {
    rep_.clear();
    rep_.resize(kHeader);
}

size_t WriteBatch::ApproximateSize() const { return rep_.size(); }

void WriteBatch::Put(const Slice& key, const Slice& value) {
    //WriteBatchInternal::SetCount(this, WriteBatchInternal::Count(this) + 1);
    rep_.push_back(static_cast<char>(kTypeValue));
    PutLengthPrefixedSlice(&rep_, key);
    PutLengthPrefixedSlice(&rep_, value);
}

void WriteBatch::Delete(const Slice& key) {
    //WriteBatchInternal::SetCount(this, WriteBatchInternal::Count(this) + 1);
    rep_.push_back(static_cast<char>(kTypeDeletion));
    PutLengthPrefixedSlice(&rep_, key);
}