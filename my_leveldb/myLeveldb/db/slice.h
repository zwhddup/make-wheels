//
// Created by bit on 2022/11/15.
//

#ifndef MYLEVELDB_SLICE_H
#define MYLEVELDB_SLICE_H

#include <iostream>
#include <cstring>
/**
 * 自定义 字符串
 */
class Slice {
public:
    Slice() : data_(""), size_(0) {}
    Slice(const char *d, size_t n) : data_(d), size_(n) {}
    Slice(const std::string &s) : data_(s.data()), size_(s.size()) {}
    Slice(const char *d) : data_(d), size_(strlen(d)) {}

    //~Slice() { delete[] data; }

    Slice(const Slice &) = default;
    Slice & operator=(const Slice &) = default;

    //char *data_
    const char * data() const { return data_; }
    size_t size() const { return size_; }
    bool empty() const { return size_ == 0; }

    bool operator==(const Slice &a) {
        return ((this->size_ == a.size_) &&
                (memcmp(this->data_, a.data_, this->size_) == 0));
    }

    bool operator!=(const Slice &a) { return !(*this == a); }

    char operator[](size_t n) const {
        if (n >= size_ || n < 0) {
            std::cout << "operator[](size_t n) err: " << n << std::endl;
            return '\0';
        }
        return data_[n];
    }

    void clear() {
        data_ = "";
        size_ = 0;
    }


    void remove_prefix(size_t n) {
        data_ += n;
        size_ -= n;
    }

    bool starts_with(const Slice &x) const {
        return ((size_ >= x.size_) && (memcmp(data_, x.data_, x.size_) == 0));
    }

    int compare(const Slice &a) const;

    std::string ToString() const { return std::string(data_, size_);  }

private:
    const char *data_;
    size_t size_;
};
#endif //MYLEVELDB_SLICE_H
