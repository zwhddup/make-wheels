//
// Created by bit on 2022/11/15.
//

#include "memTable.h"
#include "../util/coding.h"


/* MemTable 在初始化时 refs_ 为 0 */
MemTable::MemTable(const InternalKeyComparator &comparator)
        : comparator_(comparator), refs_(0), table_(comparator_, &rm) {}

MemTable::~MemTable() {
    //assert(refs_ == 0);

}

//length + key
static Slice GetLengthPrefixedSlice(const char *data) {
    uint32_t len;
    const char *p = data;
    p = GetVarint32Ptr(p, p + 5, &len);  // +5: we assume "p" is not corrupted
    //cout << "len: " << len << endl;
    return Slice(p, len);
}

//varLength + data
static const char * EncodeKey(std::string *scratch, const Slice &target) {
    scratch->clear();
    PutVarint32(scratch, target.size());
    scratch->append(target.data(), target.size());
    return scratch->data();
}

class MemTableIterator : public Iterator {
public:
    explicit MemTableIterator(MemTable::Table *table) : iter_(table) {}

    MemTableIterator(const MemTableIterator &) = delete;
    MemTableIterator &operator=(const MemTableIterator &) = delete;

    ~MemTableIterator() override = default;

    bool Valid() const override { return iter_.Valid(); }
    void Seek(const Slice &k) override { iter_.Seek(EncodeKey(&tmp_, k)); }
    void SeekToFirst() override { iter_.SeekToFirst(); }
    void SeekToLast() override { iter_.SeekToLast(); }
    void Next() override { iter_.Next(); }
    bool HasNext() override { iter_.HasNext(); }
    void Prev() override { iter_.Prev(); }

    Slice key() const override { return GetLengthPrefixedSlice(iter_.GetKey()); }
    Slice value() const override {
        Slice key_slice = GetLengthPrefixedSlice(iter_.GetKey());
        return GetLengthPrefixedSlice(key_slice.data() + key_slice.size());
    }

private:
    MemTable::Table::Iterator iter_;
    std::string tmp_;  // For passing to EncodeKey
};

Iterator * MemTable::NewIterator() { return new MemTableIterator(&table_); }

//LookupKey + varint32(val_size) + val
void MemTable::Add(SequenceNumber s, ValueType type,
                   const Slice &key,
                   const Slice &value) {
    //  Format of an entry is concatenation of:
    //  key_size     : varint32 of internal_key.size()
    //  key bytes    : char[internal_key.size()]
    //  value_size   : varint32 of value.size()
    //  value bytes  : char[value.size()]
    size_t key_size = key.size();
    size_t val_size = value.size();
//    cout << "key: " << key.ToString() << endl;
//    cout << "value: " << value.ToString() << endl;

    size_t internal_key_size = key_size + 8;//8 bytes = SequenceNumber(7) + ValueType(1)

    const size_t encoded_len = VarintLength(internal_key_size) +
            internal_key_size + VarintLength(val_size) + val_size;
    //封装至buf
    char *buf = rm.Allocate(encoded_len);
    //internal_key_size = key_size + 8
    //varint32(internal_key_size) + key + sequenceNumber + valueType + varint32(val_size) + val
    char *p = EncodeVarint32(buf, internal_key_size);
    std::memcpy(p, key.data(), key_size);
    p += key_size;
    EncodeFixed64(p, (s << 8) | type);//存储SequenceNumber + ValueType
    p += 8;
    p = EncodeVarint32(p, val_size);
    std::memcpy(p, value.data(), val_size);
    //cout << "insert buf: " << buf[encoded_len - 1] << endl;
    table_.Insert(buf);
}

bool MemTable::Get(const LookupKey &key, std::string *value) {
    Slice memkey = key.memtable_key();
    Table::Iterator iter(&table_);
    iter.Seek(memkey.data());
    if (iter.Valid()) {
        const char *entry = iter.GetKey();
        uint32_t key_length;
        const char *key_ptr = GetVarint32Ptr(entry, entry + 5, &key_length);
        if (comparator_.comparator.user_comparator()->Compare(Slice(key_ptr, key_length - 8),
                                                              key.user_key()) == 0) {
            const uint64_t tag = DecodeFixed64(key_ptr + key_length - 8);
            switch (static_cast<ValueType>(tag & 0xff)) {
                case kTypeValue: {
                    Slice v = GetLengthPrefixedSlice(key_ptr + key_length);
                    value->assign(v.data(), v.size());
                    return true;
                }
                case kTypeDeletion:
                    //*s = Status::NotFound(Slice());
                    return true;
            }
        }
    }
    return false;
}

int MemTable::KeyComparator::operator()(const char *aptr, const char *bptr) const {
    // Internal keys are encoded as length-prefixed strings.
    Slice a = GetLengthPrefixedSlice(aptr);//获取key
    Slice b = GetLengthPrefixedSlice(bptr);
    return comparator.Compare(a, b);
}
