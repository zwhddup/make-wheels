//
// Created by bit on 2022/11/17.
//

#ifndef MYLEVELDB_WRITEBATCH_H
#define MYLEVELDB_WRITEBATCH _H
#include "slice.h"

/**
 * 写 删
 */

class WriteBatch {

public:

    class Handler {
    public:
        virtual ~Handler() = default;
        virtual void Put(const Slice &key, const Slice &value) = 0;
        virtual void Delete(const Slice &key) = 0;//= 0 代表子类必须提供实现
    };

    WriteBatch();
    WriteBatch(const WriteBatch &) = default;
    WriteBatch & operator=(const WriteBatch &) = default;

    ~WriteBatch() = default;

    void Put(const Slice &key, const Slice &value);
    void Delete(const Slice &key);
    void Clear();

    size_t ApproximateSize() const;
    void Append(const WriteBatch &source);
    //Status Iterate(Handler *handler) const;
private:
    std::string rep_;
};


#endif //MYLEVELDB_WRITEBATCH_H
