//
// Created by bit on 2022/11/15.
//
#include "reusableMemory.h"

ReusableMemory::~ReusableMemory() {
    std::cout << "ReusableMemory::~ReusableMemory() release memory" << std::endl;
    //释放
    for (size_t i = 0; i < blocks.size(); ++i) {
        if (blocks[i] != nullptr) {
            delete[] blocks[i];
            blocks[i] = nullptr;
        }
    }
}

char * ReusableMemory::Allocate(size_t bytes) {
    if (bytes <= 0) {
        std::cout << "ReusableMemory::Allocate(size_t bytes) err: " << bytes << std::endl;
        return nullptr;
    }
    if (bytes <= alloc_bytes_remaining) {
        //内存充足
        char * result = alloc_ptr_start;
        alloc_ptr_start += bytes;
        alloc_bytes_remaining -= bytes;
        return result;
    }
    //剩余内存分配不足，添加新的内存块
    return AllocateFallback(bytes);
}

char * ReusableMemory::AllocateNewBlock(size_t block_bytes) {
    std::cout << "ReusableMemory::AllocateNewBlock allocate memory block" << std::endl;
    char * result = new char[block_bytes];
    blocks.push_back(result);
    memory_usage += sizeof(char*) + block_bytes;
    return result;
}

char * ReusableMemory::AllocateFallback(size_t bytes) {

    if (bytes > kBlockSize / 4) {
        //大块就直接分配bytes
        char *result = AllocateNewBlock(bytes);
        return result;
    }
    //否则小块，就按固定值kBlockSize分配，保留剩余的内存
    alloc_ptr_start = AllocateNewBlock(kBlockSize);
    alloc_bytes_remaining = kBlockSize;

    char *result = alloc_ptr_start;
    alloc_ptr_start += bytes;
    alloc_bytes_remaining -= bytes;
    return result;

}

void ReusableMemory::PrintMemoryInfo() const {

    std::cout << "memory usage: " << memory_usage << std::endl;
    std::cout << "remaining memory: " << alloc_bytes_remaining << std::endl;

}
