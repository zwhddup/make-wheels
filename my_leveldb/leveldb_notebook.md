## leveldb

>数据结构：lsm树

### 架构

<img src="images/image-20220911122615101.png" alt="image-20220531154418141" style="float:left;" />

### MemTable实现

> 实现数据结构：跳表

#### 跳表

>跳表全称为跳跃列表，它允许快速查询，插入和删除一个有序连续元素的数据链表。跳跃列表的平均查找和插入时间复杂度都是O(logn)。快速查询是通过维护一个多层次的链表，且每一层链表中的元素是前一层链表元素的子集（见右边的示意图）。一开始时，算法在最稀疏的层次进行搜索，直至需要查找的元素在该层两个相邻的元素中间。这时，算法将跳转到下一个层次，重复刚才的搜索，直到找到需要查找的元素为止。

<img src="images/skiplist.png">

##### 跳表搜索

1. 从顶层链表的首元素开始，从左往右搜索，直至找到一个大于或等于目标的元素，或者到达当前层链表的尾部
2. 如果该元素等于目标元素，则表明该元素已被找到
3. 如果该元素大于目标元素或已到达链表的尾部，则退回到当前层的前一个元素，然后转入下一层进行搜索

##### 跳表插入

在跳表中删除某个结点时，如果这个结点在索引中也出现了，我们除了要删除原始链表中的结点，还要删除索引中的。因为单链表中的删除操作需要拿到删除结点的前驱结点，然后再通过指针操作完成删除。所以在查找要删除的结点的时候，一定要获取前驱结点（双向链表除外）。因此跳表的删除操作时间复杂度即为O(logn)。

##### 跳表更新索引

当我们不断地往跳表中插入数据时，我们如果不更新索引，就有可能出现某2个索引节点之间的数据非常多的情况，在极端情况下，跳表还会退化成单链表。

跳表是通过随机函数来维护“平衡性”。

当我们在跳表中插入数据的时候，我们通过选择同时将这个数据插入到部分索引层中，如何选择索引层，可以通过一个随机函数来决定这个节点插入到哪几级索引中，比如随机生成了k，那么就将这个索引加入到，第一级到第k级索引中。

##### skipList存储的key

```c++
// Format of an entry is concatenation of:
//  key_size     : varint32 of internal_key.size()
//  key bytes    : char[internal_key.size()]
//  value_size   : varint32 of value.size()
//  value bytes  : char[value.size()]
void MemTable::Add(SequenceNumber s, ValueType type,
                   const Slice &key,
                   const Slice &value) {
    // Format of an entry is concatenation of:
    //  key_size     : varint32 of internal_key.size()
    //  key bytes    : char[internal_key.size()]
    //  value_size   : varint32 of value.size()
    //  value bytes  : char[value.size()]
    size_t key_size = key.size();
    size_t val_size = value.size();

    size_t internal_key_size = key_size + 8;//8 bytes = SequenceNumber + ValueType

    const size_t encoded_len = VarintLength(internal_key_size) +
            internal_key_size + VarintLength(val_size) + val_size;
    //封装至buf
    char *buf = rm.Allocate(encoded_len);

    char *p = EncodeVarint32(buf, internal_key_size);
    std::memcmp(p, key.data(), key_size);
    p += key_size;
    EncodeFixed64(p, (s << 8) | type);//存储SequenceNumber + ValueType
    p += 8;

    p = EncodeVarint32(p, val_size);
    std::memcmp(p, value.data(), val_size);
    table_.Insert(buf);
}
```

### 内存分配器

#### placement new机制

所谓placement new就是在用户指定的内存位置上构建新的对象，这个构建过程不需要额外分配内存，只需要调用对象的构造函数即可。
举例来说:

```c++
class Foo{};
Foo *pfoo = new Foo;
```

pfoo指向的对象的地址你是不能决定的，因为new已经为你做了这些工作。第一步分配内存，第二步调用类的构造函数。而placement new是怎么做的呢，说白了就是把原本new做的两步工作分开来。第一步你自己分配内存，第二步你调用类的构造函数在自己分配的内存上构建新的对象。

```c++
char *buff = new char[sizeof(Foo)]
Foo *foo = new (buff) Foo;
```

### 比较器 comparator

### 自定义字符串

**Slice**

### 编码

可变长**VarInt32、VarInt64**，缩减存储

如：

VarInt32 (vary int 32)，即：长度可变的 32 为整型类型。一般来说，int 类型的长度固定为 32 字节。但 VarInt32类型的数据长度是不固定的，VarInt32 中每个字节的最高位有特殊的含义。如果最高位为 1 代表下一个字节也是该数字的一部分。因此，表示一个整型数字最少用 1 个字节，最多用 5 个字节表示。如果某个系统中大部分数字需要 >= 4 字节才能表示，那其实并不适合用 VarInt32 来编码。

```c++
//以 129 为例，它的二进制为 1000 0001 。
//由于每个字节最高位用于特殊标记，因此只能有 7 位存储数据。
//第一个字节存储最后 7 位 （000 0001），但并没有存下所有的比特，因此最高位置位 1，剩下的部分用后续字节表示。所以，第一个字节为：1000 0001
//第二个字节只存储一个比特位即可，因此最高位为 0 ，所以，第二个字节为：0000 0001
//这样，我们就不必用 4 字节的整型存储 129 ，可以节省存储空间
char * EncodeVarint32(char *dst, uint32_t v) {
    // Operate on characters as unsigneds
    uint8_t *ptr = reinterpret_cast<uint8_t *>(dst);
    static const int B = 128;//10000000
    /**
     * 01001010 10101010
    */
    if (v < (1 << 7)) {//10000000
        *(ptr++) = v;
    } else if (v < (1 << 14)) {// 1000000 00000000
        *(ptr++) = v | B;
        *(ptr++) = v >> 7;
    } else if (v < (1 << 21)) {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = v >> 14;
    } else if (v < (1 << 28)) {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = (v >> 14) | B;
        *(ptr++) = v >> 21;
    } else {
        *(ptr++) = v | B;
        *(ptr++) = (v >> 7) | B;
        *(ptr++) = (v >> 14) | B;
        *(ptr++) = (v >> 21) | B;
        *(ptr++) = v >> 28;
    }
    return reinterpret_cast<char *>(ptr);
}
```

### Put()、Delete()



### 参考资料

https://15721.courses.cs.cmu.edu/spring2018/papers/08-oltpindexes1/pugh-skiplists-cacm1990.pdf

[protobuf int类型变长存储 varint32 和 varint64 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/446399148)

