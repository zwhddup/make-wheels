#ifndef TCPSERVER_HPP_
#define TCPSERVER_HPP_

#define _WINSOCK_DEPRECATED_NO_WARNINGS
//windows环境
#ifdef _WIN32
    #define FD_SETSIZE (1024 * 2)
    #define WIN32_LEAN_AND_MEAN //使下面两个文件没有引入顺序限制
    #include <Windows.h>
    #include <WinSock2.h>
#else
    #include <unistd.h>
    #include <arpa/inet.h>
    #define SOCKET int
    #define INVALID_SOCKET (SOCKET)(~0)
    #define SOCKET_ERROR (-1)
#endif
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <thread>
#include <mutex>
#include <atomic>
#include <functional>
#include "MessageHeader.hpp"
#include  "CELLTimestamp.hpp"

#ifndef RECV_BUFF_SIZE
#define RECV_BUFF_SIZE (10240)
#endif

#define _CELLSERVER_THREAD_COUNT (8)

//客户端数据类型
class ClientSocket {
public:
    ClientSocket(SOCKET sockfd = INVALID_SOCKET) : _sockfd(sockfd) {
        memset(_szMsgBuf, 0, sizeof(_szMsgBuf));
        _lastPos = 0;
    }
    SOCKET sockfd() {
        return _sockfd;
    }
    char* msgBuf() {
        return _szMsgBuf;
    }
    int getLastPos() {
        return _lastPos;
    }
    void setLastPos(int pos) {
        _lastPos = pos;
    }
private:
    SOCKET _sockfd;
    char _szMsgBuf[RECV_BUFF_SIZE * 10];//第二缓冲区
    int _lastPos;
};

//网络事件接口
class INetEvent {
public:
    virtual void OnLeave(ClientSocket* pClient) = 0;
    virtual void OnNetMsg(SOCKET cSock, DataHeader* header) = 0;
private:
};

class CellServer {
public:
    CellServer(SOCKET sock = INVALID_SOCKET) {
        memset(_szRecv, 0, sizeof(_szRecv));
        _sock = sock;
        _pthread = nullptr;
        _pNetEvent = nullptr;
    }
    virtual ~CellServer() {
        if (_pthread != nullptr) {
            delete _pthread;
            _pthread = nullptr;
        }
        Close();
    }

    void SetEventObj(INetEvent* iNetEvent) {
        _pNetEvent = iNetEvent;
    }

    bool OnRun() {
        while (IsRun()) {
            if (_clientsBuff.size() > 0) {
                std::lock_guard<std::mutex> lock(_mutex);
                for (auto pClient : _clientsBuff) {
                    _clients.push_back(pClient);
                }
                _clientsBuff.clear();
            }
            if (_clients.empty()) {
                std::chrono::milliseconds t(1);
                std::this_thread::sleep_for(t);
                continue;
            }
            //伯克利套接字 BSD socket
            fd_set fdRead;//描述符(socket)集合
            //清理集合
            FD_ZERO(&fdRead);
            SOCKET maxSock = _clients[0]->sockfd();
            for (size_t n = 0; n < _clients.size(); ++n) {
                FD_SET(_clients[n]->sockfd(), &fdRead);
                if (maxSock < _clients[n]->sockfd()) {
                    maxSock = _clients[n]->sockfd();
                }
            }
            //nfds是一个整数值，是指fd_set集合中所有描述符的范围，而不是数量，即最大的socket描述符加1
            //windows这个参数可以写0
            //select 最后一个参数为NULL，select会阻塞等待事件
            timeval t = {0, 0};
            int ret = select(maxSock + 1, &fdRead, nullptr, nullptr, &t);
            if (ret < 0) {
                std::cout << "select function end." << std::endl;
                Close();
                return false;
            }
            //处理各个客户端数据
            for (size_t n = 0; n < _clients.size(); ++n) {
                if (FD_ISSET(_clients[n]->sockfd(), &fdRead)) {
                    FD_CLR(_clients[n]->sockfd(), &fdRead);
                    if (-1 == RecvData(_clients[n])) {
                        auto iter = _clients.begin() + n;
                        if (iter != _clients.end()) {
                            if (_pNetEvent) {
                                _pNetEvent->OnLeave(_clients[n]);
                            }
                            if (_clients[n] != nullptr) {
                                delete _clients[n];
                                _clients[n] = nullptr;
                            }
                            _clients.erase(iter);
                        }
                    }
                }
            }
        }

    }
    bool IsRun() {
        return _sock != INVALID_SOCKET;
    }
    void Close() {
        if (this->_sock != INVALID_SOCKET) {
#ifdef _WIN32
            for (size_t n = 0; n < _clients.size(); ++n) {
                closesocket(_clients[n]->sockfd());
                if (_clients[n] != nullptr) {
                    delete _clients[n];
                    _clients[n] = nullptr;
                }
            }
            closesocket(_sock);
#else
            for (size_t n = 0; n < _clients.size(); ++n) {
                close(_clients[n]->sockfd());
                if (_clients[n] != nullptr) {
                    delete _clients[n];
                    _clients[n] = nullptr;
                }
            }
            close(_sock);
#endif
            if (!_clients.empty()) {
                _clients.clear();
            }
            _sock = INVALID_SOCKET;
        }
    }
    //handle client cmd
    int RecvData(ClientSocket* cSock) {
        int nLen = recv(cSock->sockfd(), _szRecv, RECV_BUFF_SIZE, 0);
        if (nLen <= 0) {
            printf("client %d  had exited. Total clients: %d\n", cSock->sockfd(), _clients.size());
            return -1;
        }
        //接收缓冲区拷贝到第二缓冲区
        memcpy(cSock->msgBuf() + cSock->getLastPos(), _szRecv, nLen);
        cSock->setLastPos(cSock->getLastPos() + nLen);
        //handle request package
        while (cSock->getLastPos() >= sizeof(DataHeader)) {
            DataHeader* header = (DataHeader*)cSock->msgBuf();
            if (cSock->getLastPos() >= header->dataLength) {
                int nSize = cSock->getLastPos() - header->dataLength;
                //printf("SOCKET<%d> recv cmd: %d dataLength: %d\n", _cSock, header->cmd, header->dataLength);
                OnNetMsg(cSock->sockfd(), header);
                memcpy(cSock->msgBuf(), cSock->msgBuf() + header->dataLength,nSize);
                cSock->setLastPos(nSize);
            }
            else {
                break;
            }
        }
        return 0;
    }

    virtual void OnNetMsg(SOCKET cSock, DataHeader* header) {
        _pNetEvent->OnNetMsg(cSock, header);
        switch (header->cmd) {
            case CMD_LOGIN: {
                Login* login = (Login*)header;
                //printf("SOCKET<%d> userName: %s password: %s\n", _cSock, login->userName, login->password);
                //...
                LoginResult ret;
                //SendData(cSock, &ret);
            }
                break;
            case CMD_LOGOUT: {
                Logout* logout = (Logout*)header;
                //printf("SOCKET<%d> userName: %s\n", _cSock, logout->userName);
                //...
                LogoutResult ret;
                //SendData(cSock, &ret);
            }
                break;
            default: {
                header->cmd = CMD_ERROR;
                header->dataLength = 0;
                //SendData(cSock, header);
            }
                break;
        }
    }

    void AddClient(ClientSocket* pClient) {
        std::lock_guard<std::mutex> lock(_mutex);//自解锁 耗时
        _clientsBuff.push_back(pClient);
    }

    void Start() {
        _pthread = new std::thread(std::mem_fn(&CellServer::OnRun), this);
    }

    size_t GetClientCount() {
        return _clients.size() + _clientsBuff.size();
    }
    char _szRecv[RECV_BUFF_SIZE];//接收缓冲区
private:
    SOCKET _sock;
    std::vector<ClientSocket*> _clients;//正式客户队列
    std::vector<ClientSocket*> _clientsBuff;//缓冲客户队列
    std::thread* _pthread;
    std::mutex _mutex;//缓冲客户队列 操作锁
    CELLTimestamp _tTime;//计时器
    INetEvent* _pNetEvent;//网络事件对象
};

class TCPServer : public INetEvent {
public:
    SOCKET _sock;
    std::vector<CellServer*> _cellServers;
    CELLTimestamp _tTime;//计时器
    std::atomic_int _recvCount;//收到消息的计数

	TCPServer() {
        _sock = INVALID_SOCKET;
        _recvCount = 0;
	}
	~TCPServer() {
        Close();
    }
    void InitSocket() {
#ifdef _WIN32
	    WORD ver = MAKEWORD(2, 2);
        WSADATA dat;
        WSAStartup(ver, &dat);
#endif
        if (INVALID_SOCKET != _sock) {
            std::cout << "close old socket." << std::endl;
#ifdef _WIN32
            closesocket(_sock);
#else
            close(_sock);
#endif
        }
        _sock = socket(AF_INET, SOCK_STREAM, 0);
        if (INVALID_SOCKET == _sock) {
            std::cout << "socket build failed" << std::endl;
        }
    }
    int Bind(const char* ip, unsigned short port) {
        if (INVALID_SOCKET == _sock) {
            InitSocket();
        }
        sockaddr_in _sin = {};
        _sin.sin_family = AF_INET;
        _sin.sin_port = htons(port);
        if (ip) {
            _sin.sin_addr.s_addr = inet_addr(ip);
        }
        else {
            _sin.sin_addr.s_addr = INADDR_ANY;
        }
        int ret = bind(_sock, (sockaddr*)&_sin, sizeof(sockaddr_in));
        if (ret == SOCKET_ERROR) {
            std::cout << "bind failed" << std::endl;
        }
        return ret;
    }
    int Listen(int n) {
        int ret = listen(_sock, n);
        if (ret == SOCKET_ERROR) {
            std::cout << _sock << " listen failed" << std::endl;
        }
        std::cout << _sock << " server listening" << std::endl;
        return ret;
    }
    SOCKET Accept() {
        sockaddr_in clientAddr = {};
        int nAddrLen = sizeof(sockaddr_in);
        SOCKET cSock = INVALID_SOCKET;
#ifdef _WIN32
        cSock = accept(_sock, (sockaddr*)&clientAddr, &nAddrLen);
#else
        cSock = accept(_sock, (sockaddr*)&clientAddr, (socklen_t*)&nAddrLen);
#endif
        if (cSock == INVALID_SOCKET) {
            std::cout << "accept error client socket" << std::endl;
        }
        else {
            AddClientToCellServer(new ClientSocket(cSock));
            //inet_ntoa(clientAddr.sin_addr)
        }
        return cSock;
    }
    //线程的负载均衡
    void AddClientToCellServer(ClientSocket* pClient) {
        auto pMinServer = _cellServers[0];
        for (auto pCellServer : _cellServers) {
            if (pMinServer->GetClientCount() > pCellServer->GetClientCount()) {
                pMinServer = pCellServer;
            }
        }
        pMinServer->AddClient(pClient);
    }
    //创建客户端数据处理线程
    void Start() {
        for (int n = 0; n < _CELLSERVER_THREAD_COUNT; ++n) {
            auto ser = new CellServer(_sock);
            _cellServers.push_back(ser);
            //注册网络事件接受对象
            ser->SetEventObj(this);
            ser->Start();
        }
    }

    bool OnRun() {
        if (IsRun()) {
            Time4msg();
            //伯克利套接字 BSD socket
            fd_set fdRead;//描述符(socket)集合
            //fd_set fdWrite;
            //fd_set fdExp;
            //清理集合
            FD_ZERO(&fdRead);
            //FD_ZERO(&fdWrite);
            //FD_ZERO(&fdExp);
            //加入集合
            FD_SET(_sock, &fdRead);
            //FD_SET(_sock, &fdWrite);
            //FD_SET(_sock, &fdExp);
            //nfds是一个整数值，是指fd_set集合中所有描述符的范围，而不是数量，即最大的socket描述符加1
            //windows这个参数可以写0
            //select 最后一个参数为NULL，select会阻塞等待事件
            timeval t = {0, 0};
            int ret = select(_sock + 1, &fdRead, nullptr, nullptr, &t);
            if (ret < 0) {
                std::cout << "select function end." << std::endl;
                Close();
                return false;
            }
            if (FD_ISSET(_sock, &fdRead)) {//判断描述符是否在集合中
                FD_CLR(_sock, &fdRead);
                Accept();
            }
        }
        return true;
    }
    bool IsRun() {
        return _sock != INVALID_SOCKET;
    }
    void Close() {
        if (this->_sock != INVALID_SOCKET) {
#ifdef _WIN32
            closesocket(_sock);
            WSACleanup();
#else
            close(_sock);
#endif
            _sock = INVALID_SOCKET;
        }
    }

    void Time4msg() {
        auto t1 = _tTime.getElapsedSecond();
        if (t1 >= 1.0) {
            printf("thread<%d>, time<%lf>, socket<%d>, _recvCount/s<%d>\n", _cellServers.size(), t1, _sock, int(_recvCount / t1));
            _recvCount = 0;
            _tTime.update();
        }
    }

    int SendData(SOCKET cSock, DataHeader* header) {
        if (IsRun() && header) {
            return send(cSock, (const char*)header, header->dataLength, 0);
        }
        return SOCKET_ERROR;
    }

    virtual void OnLeave(ClientSocket* pClient) {

    }

    virtual void OnNetMsg(SOCKET cSock, DataHeader* header) {
        ++_recvCount;
    }
private:
};
#endif