#ifndef CELLTIMESSTAMP_HPP
#define CELLTIMESSTAMP_HPP
#include <chrono>
//��ʱ��
using namespace std::chrono;
class CELLTimestamp {
public:
	CELLTimestamp() {
		update();
	}
	~CELLTimestamp() {

	}
	void update() {
		_begin = high_resolution_clock::now();//����
	}
	double getElapsedSecond() {
		return getElapsedTimeInMicroSec() * 0.000001;
	}
	double getElapsedTimeInMilliSec() {
		return getElapsedTimeInMilliSec() * 0.001;
	}
	long long getElapsedTimeInMicroSec() {
		return duration_cast<microseconds>(high_resolution_clock::now() - _begin).count();
	}
protected:
	time_point<high_resolution_clock> _begin;
};
#endif // !CELLTIMESSTAMP_HPP
