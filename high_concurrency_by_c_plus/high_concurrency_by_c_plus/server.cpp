#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include "TCPServer.hpp"
#include <thread>
#include <cstring>
//ѧϰ��ַ
//https://www.bilibili.com/video/BV1o5411A7a7/?p=44&share_source=copy_web&vd_source=6164cc1e15b15d47186e6ecfe12edef8
//g++ -std=c++11 -o server server.cpp -pthread
bool run = true;
void cmdThread() {
    while (true) {
        char cmdBuf[128] = {};
        std::cin >> cmdBuf;
        if (0 == strcmp(cmdBuf, "exit")) {
            run = false;
            break;
        }
    }
    std::cout << "cmdThread exit." << std::endl;
}

int main()  {
    TCPServer* tcpServer = new TCPServer();
    std::thread t1(cmdThread);
    t1.detach();
    tcpServer->InitSocket();
    tcpServer->Bind(NULL, 8080);
    tcpServer->Listen(5);
    tcpServer->Start();
    while (run) {
        tcpServer->OnRun();
    }
    tcpServer->Close();
    if (tcpServer != nullptr) {
        delete tcpServer;
        tcpServer = nullptr;
    }
    return 0;
}