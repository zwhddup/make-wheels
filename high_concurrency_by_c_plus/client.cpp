#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN //使下面两个文件没有引入顺序限制
#include <Windows.h>
#include <WinSock2.h>
#include <iostream>

int main() {
	WORD ver = MAKEWORD(2, 2);
    WSADATA dat;
    WSAStartup(ver, &dat);
    SOCKET _sock = socket(AF_INET, SOCK_STREAM, 0);
    if (INVALID_SOCKET == _sock) {
        std::cout << "socket build failed" << std::endl;
    }
    sockaddr_in _sin = {};
    _sin.sin_family = AF_INET;
    _sin.sin_port = htons(8080);
    _sin.sin_addr.S_un.S_addr =  inet_addr("127.0.0.1");
    
    int ret = connect(_sock, (sockaddr*)&_sin, sizeof(sockaddr_in));
    if (ret == SOCKET_ERROR) {
        std::cout << "socket connect failed" << std::endl;
    }
    std::cout << "client connect successed" << std::endl;
    while (true) {
        char cmdBuf[128] = {};
        std::cin >> cmdBuf;
        if (0 == strcmp(cmdBuf, "exit")) {
            break;
        }
        else {
            send(_sock, cmdBuf, strlen(cmdBuf) + 1, 0);
        }
        char recvBuf[256] = {};
        int nlen = recv(_sock, recvBuf, 256, 0);
        if (nlen > 0) {
            std::cout << "recv data: " << recvBuf << std::endl;
        }
    }
    closesocket(_sock);
    WSACleanup();
    getchar();
    return 0;
}