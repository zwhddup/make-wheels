#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include <thread>
#include "TCPClient.hpp"
//g++ -std=c++11 -o client client.cpp -pthread
bool run = true;
void cmdThread() {
    while (true) {
        char cmdBuf[128] = {};
        std::cin >> cmdBuf;
        if (0 == strcmp(cmdBuf, "exit")) {
            run = false;
            break;
        }
    }
    std::cout << "cmdThread exit." << std::endl;
}

//FD_SETSIZE select函数最大处理的事件集合大小
//const int cCount = FD_SETSIZE - 1;
const int cCount = 10000;//总客户端数
const int tCount = 8;//发送线程数量
TCPClient* tcpClients[cCount];

void sendThread(int id) {
    int c = cCount / tCount;
    int begin = (id - 1) * c;
    int end = id * c;
    for (int i = begin; i < end; ++i) {
        tcpClients[i] = new TCPClient();
    }
    for (int i = begin; i < end; ++i) {
        tcpClients[i]->ConnectServer("127.0.0.1", 8080);//8.130.83.110  
    }
    Login login[10];
    for (int i = 0; i < 10; ++i) {
        strcpy(login[i].userName, "xiaoming");
        strcpy(login[i].password, "12345678");
    }
    std::chrono::microseconds t(5000);
    std::this_thread::sleep_for(t);
    int nLen = sizeof(login);
    while (run) {
        for (int i = begin; i < end; ++i) {
            tcpClients[i]->SendData(login, nLen);
            tcpClients[i]->OnRun();
        }
    }
    for (int i = begin; i < end; ++i) {
        tcpClients[i]->Close();
        if (tcpClients[i] != nullptr) {
            delete tcpClients[i];
            tcpClients[i] = nullptr;
        }
    }
}

int main() {
    std::thread t1(cmdThread);
    t1.detach();

    for (int i = 0; i < tCount; ++i) {
        std::thread t1(sendThread, i + 1);
        t1.detach();
    }
    while (run) {
        Sleep(100);
    }
    return 0;
}