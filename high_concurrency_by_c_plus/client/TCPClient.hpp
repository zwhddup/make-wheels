#ifndef TCPCLIENT_HPP_
#define TCPCLIENT_HPP_
//windows环境
#ifdef _WIN32
    #define WIN32_LEAN_AND_MEAN //使下面两个文件没有引入顺序限制
    #include <Windows.h>
    #include <WinSock2.h>
    //#pragma comment(lib, "ws2_32.lib")
//linux环境
#else
    #include <unistd.h>
    #include <arpa/inet.h>
    #define SOCKET int
    #define INVALID_SOCKET (SOCKET)(~0)
    #define SOCKET_ERROR (-1)
#endif
#include <iostream>
#include <cstdio>
#include <cstring>
#include "MessageHeader.hpp"
#define RECV_BUFF_SIZE (10240)

class TCPClient {
public:
	SOCKET _sock;
    char _szRecv[RECV_BUFF_SIZE];//接收缓冲区
    char _szMsgBuf[RECV_BUFF_SIZE * 10];//第二缓冲区
    int _lastPos;

	TCPClient() {
        memset(_szRecv, 0, sizeof(_szRecv));
        memset(_szMsgBuf, 0, sizeof(_szMsgBuf));
        _sock = INVALID_SOCKET;
        _lastPos = 0;
	}

	virtual ~TCPClient() {
        Close();
	}

	void InitSocket() {
#ifdef _WIN32
	    WORD ver = MAKEWORD(2, 2);
        WSADATA dat;
        WSAStartup(ver, &dat);
#endif
        if (INVALID_SOCKET != _sock) {
            std::cout << "close old socket." << std::endl;
#ifdef _WIN32
            closesocket(_sock);
#else
            close(_sock);
#endif
        }
        _sock = socket(AF_INET, SOCK_STREAM, 0);
        if (INVALID_SOCKET == _sock) {
            std::cout << "socket build failed" << std::endl;
        }
	}

	int ConnectServer(const char* ip, short port) {
        if (INVALID_SOCKET == _sock) {
            InitSocket();
        }
        sockaddr_in _sin = {};
        _sin.sin_family = AF_INET;
        _sin.sin_port = htons(port);
        _sin.sin_addr.s_addr =  inet_addr(ip);
        int ret = connect(_sock, (sockaddr*)&_sin, sizeof(sockaddr_in));
        if (ret == SOCKET_ERROR) {
            std::cout << "socket connect failed" << std::endl;
        }
        else {
            printf("client %d connect successed\n", _sock);
        }
        return ret;
	}

	void Close() {
        if (this->_sock != INVALID_SOCKET) {
#ifdef _WIN32
            closesocket(_sock);
            WSACleanup();
#else
            close(_sock);
#endif
            _sock = INVALID_SOCKET;
        }
	}

    bool OnRun() {
        if (IsRun()) {
            fd_set fdReads;
            FD_ZERO(&fdReads);
            FD_SET(_sock, &fdReads);
            timeval t = {0, 0};
            int ret = select(_sock + 1, &fdReads, NULL, NULL, &t);
            if (ret < 0) {
                std::cout << "select function end." << std::endl;
                Close();
                return false;
            }
            if (FD_ISSET(_sock, &fdReads)) {
                FD_CLR(_sock, &fdReads);
                if (RecvData() == -1) {
                    std::cout << "processor function end." << std::endl;
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    bool IsRun() {
        return _sock != INVALID_SOCKET;
    }

    int RecvData() {
        int nLen = recv(_sock, _szRecv, RECV_BUFF_SIZE, 0);
        if (nLen <= 0) {
            std::cout << "disconnect from the server" << std::endl;
            return -1;
        }
        //接收缓冲区拷贝到第二缓冲区
        memcpy(_szMsgBuf + _lastPos, _szRecv, nLen);
        _lastPos += nLen;
        //handle request package
        while (_lastPos >= sizeof(DataHeader)) {
            DataHeader* header = (DataHeader*)_szMsgBuf;
            if (_lastPos >= header->dataLength) {
                int nSize = _lastPos - header->dataLength;
                //printf("SOCKET<%d> recv cmd: %d dataLength: %d\n", _sock, header->cmd, header->dataLength);
                OnNetMsg(header);
                memcpy(_szMsgBuf, _szMsgBuf + header->dataLength, nSize);
                _lastPos = nSize;
            }
            else {
                break;
            }
        }
        return 0;
    }

    //处理网络消息
    virtual void OnNetMsg(DataHeader* header) {
        switch (header->cmd) {
            case CMD_LOGIN_RESULT: {
                LoginResult* loginRet = (LoginResult*)header;
                //std::cout << "loginRet: " << loginRet->result << std::endl;
            }
                break;
            case CMD_LOGOUT_RESULT: {
                LogoutResult* logoutRet = (LogoutResult*)header;
                //std::cout << "logoutRet: " << logoutRet->result << std::endl;
            }
                break;
            case CMD_NEW_USER_JOIN: {
                NewUserJoin* userJoin = (NewUserJoin*)header;
                //std::cout << "userJoin: " << userJoin->sock << std::endl;
            }
                break;
            case CMD_ERROR:
                printf("SOCKET<%d> send error cmd\n", _sock);
                break;
            default: {
                printf("SOCKET<%d> recv error message, data length: %d\n", _sock, header->dataLength);
            }
        }
    }

    int SendData(DataHeader* header, int nLen) {
        if (IsRun() && header) {
            return send(_sock, (const char*)header, nLen, 0);
        }
        return SOCKET_ERROR;
    }
private:
};
#endif


